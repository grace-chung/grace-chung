# H i  ! &nbsp; I ' m  &nbsp; G r a c e 👋

- I'm a frontend developer with experience in JavaScript (TypeScript | React).

- Looking forward to using new technologies and diving into backend development.

- Previously at Shopify 🛍️.


